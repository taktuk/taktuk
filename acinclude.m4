dnl Define macros to check programs
AC_DEFUN([PROG_POD2MAN], [ACX_PROG_PROG([POD2MAN],[pod2man])]) 
AC_DEFUN([PROG_POD2HTML], [ACX_PROG_PROG([POD2HTML],[pod2html])]) 
AC_DEFUN([PROG_PERL], [ACX_PROG_PROG([PERL],[perl])]) 
