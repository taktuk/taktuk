TAKTUK_CONNECTOR='oarsh -o StrictHostKeyChecking=no -o BatchMode=yes'
export TAKTUK_CONNECTOR

get_hosts ()
  {
    if [ -n "$OAR_NODEFILE" ]
    then
      printf "%s\n" "-f $OAR_NODEFILE"
    else
      printf "%s\n" \
         "Unable to find remote peers specification (are you using OAR ?)" >&2
      printf "%s\n" ""
    fi
  }
