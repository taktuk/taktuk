#!/usr/bin/perl -w
###############################################################################
#                                                                             #
#  TakTuk, a middleware for adaptive large scale parallel remote executions   #
#  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        #
#                                                                             #
#  This program is free software; you can redistribute it and/or modify       #
#  it under the terms of the GNU General Public License as published by       #
#  the Free Software Foundation; either version 2 of the License, or          #
#  (at your option) any later version.                                        #
#                                                                             #
#  This program is distributed in the hope that it will be useful,            #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#  GNU General Public License for more details.                               #
#                                                                             #
#  You should have received a copy of the GNU General Public License          #
#  along with this program; if not, write to the Free Software                #
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA #
#                                                                             #
#  Contact: Guillaume.Huard@imag.fr                                           #
#           Laboratoire LIG - ENSIMAG - Antenne de Montbonnot                 #
#           51 avenue Jean Kuntzmann                                          #
#           38330 Montbonnot Saint Martin                                     #
#           FRANCE                                                            #
#                                                                             #
###############################################################################

use strict;

my $rank = TakTuk::get('rank');
my $count = TakTuk::get('count');

print "I'm process $rank among $count (new version for 3.6)\n";

if ($rank > 1)
  {
    my ($from, $message) = TakTuk::recv();
    if (not defined($message))
      {
        print "Trying to recv: ",TakTuk::error_msg($TakTuk::error), "\n";
      }
    else
      {
        print "received $message from $from\n";
      }
  }

sleep 1;
my $next = $rank+1;
$next = 1 if ($next > $count);
if (not TakTuk::send(to=>$next,body=>"[Salut numero $rank]"))
  {
    print "Trying to send to $next: ",TakTuk::error_msg($TakTuk::error), "\n";
  }

if ($rank == 1)
  {
    my ($from, $message) = TakTuk::recv(timeout=>5);
    if (not defined($message))
      {
        print "Trying to recv :", TakTuk::error_msg($TakTuk::error), "\n";
      }
    else
      {
        print "received $message from $from\n";
      }
  }
