echo_pipe_commands ()
  {
    echo broadcast exec [ 'cat >/tmp/destination.taktuk' ]
    echo broadcast input pipe [ /tmp/source.taktuk ]
    sleep 4
    echo quit
  }
