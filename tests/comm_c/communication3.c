/******************************************************************************
*                                                                             *
*  TakTuk, a middleware for adaptive large scale parallel remote executions   *
*  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        *
*                                                                             *
*  This program is free software; you can redistribute it and/or modify       *
*  it under the terms of the GNU General Public License as published by       *
*  the Free Software Foundation; either version 2 of the License, or          *
*  (at your option) any later version.                                        *
*                                                                             *
*  This program is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with this program; if not, write to the Free Software                *
*  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA *
*                                                                             *
*  Contact: Guillaume.Huard@imag.fr                                           *
*           ENSIMAG - Laboratoire d'Informatique de Grenoble                  *
*           51 avenue Jean Kuntzmann                                          *
*           38330 Montbonnot Saint Martin                                     *
*                                                                             *
******************************************************************************/

#include <taktuk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
  {
    char buffer[1024];
    int length, i;
    char sample_string[128];
    unsigned long rank, from, new_rank;
    int result;

    if (argc != 2)
      {
        fprintf(stderr, "This command require exactly one numeric argument\n");
        exit(1);
      }
    result = taktuk_get("rank", &rank);
    if (result)
      {
        fprintf(stderr,"Invalid rank: %s, do you use TakTuk ?\n",
                taktuk_error_msg(result));
        return 1;
      }
    printf("I'm process %lu\n", rank);

    for (i=0; i<atoi(argv[1]); i++)
      {
        if (rank == 1)
          {
            sprintf(sample_string, "Ping %d", i);
            length = strlen(sample_string)+1;
            result = taktuk_send(2, TAKTUK_TARGET_ANY, sample_string, length);
            if (result)
              {
                printf("Send error in %lu : %s\n", rank,
                       taktuk_error_msg(result));
              }
            else
              {
                printf("Sent %s\n", sample_string);
                result = taktuk_recv(&from, buffer, NULL, NULL);
                if (result)
                  {
                    printf("Recv error in %lu : %s\n", rank,
                           taktuk_error_msg(result));
                  }
                else
                  {
                    printf("Received %s from %lu\n", buffer, from);
                  }
              }
            /* Recomputes rank only for stability testing */
            result = taktuk_get("rank", &new_rank);
            if (result)
              {
                printf("Rank error in %lu : %s\n", rank,
                       taktuk_error_msg(result));
              }
            if (rank != new_rank)
                printf("RANK CHANGED !!!\n");
            else
                printf("rank ok\n");
          }
        else if (rank == 2)
          {
            sprintf(sample_string, "Pong %d", i);
            length = strlen(sample_string)+1;
            result = taktuk_recv(&from, buffer, NULL, NULL);
            if (result)
              {
                printf("Recv error in %lu : %s\n", rank,
                       taktuk_error_msg(result));
              }
            else
              {
                printf("Received %s from %lu\n", buffer, from);
                result = taktuk_send(1, TAKTUK_TARGET_ANY, sample_string,
                                                                       length);
                if (result)
                  {
                    printf("Send error in %lu : %s\n", rank,
                           taktuk_error_msg(result));
                  }
                else
                  {
                    printf("Sent %s\n", sample_string);
                  }
              }
            /* Recomputes rank only for stability testing */
            result = taktuk_get("rank", &new_rank);
            if (result)
              {
                printf("Rank error in %lu : %s\n", rank,
                       taktuk_error_msg(result));
              }
            if (rank != new_rank)
                printf("RANK CHANGED !!!\n");
            else
                printf("rank ok\n");
          }
        else
          {
            printf("Got nothing to do\n");
          }
      }
    return 0;
  }
