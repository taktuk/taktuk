/******************************************************************************
*                                                                             *
*  TakTuk, a middleware for adaptive large scale parallel remote executions   *
*  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        *
*                                                                             *
*  This program is free software; you can redistribute it and/or modify       *
*  it under the terms of the GNU General Public License as published by       *
*  the Free Software Foundation; either version 2 of the License, or          *
*  (at your option) any later version.                                        *
*                                                                             *
*  This program is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with this program; if not, write to the Free Software                *
*  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA *
*                                                                             *
*  Contact: Guillaume.Huard@imag.fr                                           *
*           ENSIMAG - Laboratoire d'Informatique de Grenoble                  *
*           51 avenue Jean Kuntzmann                                          *
*           38330 Montbonnot Saint Martin                                     *
*                                                                             *
******************************************************************************/

#include <taktuk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main()
  {
    char first_part[128];
    char second_part[128];
    struct iovec vecteur[2];
    unsigned long rank, count, from, next;
    int result;
    struct timeval timeout = { 5, 500000 };
    
    vecteur[0].iov_base = first_part;
    vecteur[0].iov_len = 128;
    vecteur[1].iov_base = second_part;
    vecteur[1].iov_len = 128;

    result = taktuk_get("rank", &rank);
    if (!result)
        result = taktuk_get("count", &count);
    if (result)
      {
        fprintf(stderr, "Invalid rank or count: %s, do you use TakTuk ?\n",
                taktuk_error_msg(result));
        return 1;
      }

    printf("I'm process %lu among %lu\n", rank, count);

    // Receive for all except 1
    if (rank > 1)
      {
        result = taktuk_recvv(&from, vecteur, 2, NULL);
        if (result)
          {
            printf("Error %d in recv %lu : %s\n", result, rank,
                   taktuk_error_msg(result));
          }
        else
          {
            printf("Received [%s] and [%s] from %lu\n",
                   (char *) vecteur[0].iov_base,
                   (char *) vecteur[1].iov_base, from);
          }
      }
    else
      {
        strcpy(first_part, "Salut, toi ");
        strcpy(second_part, "de 1");
      }

    // Send for all
    sleep(1);
    next = (rank%count)+1;
    result = taktuk_sendv(next, TAKTUK_TARGET_ANY, vecteur, 2);
    if (result)
      {
        printf("Error %d in send %lu : %s\n",result,rank,
               taktuk_error_msg(result));
      }
    
    if (rank == 1)
      {
        result = taktuk_recvv(&from, vecteur, 2, &timeout);
        if (result)
          {
            printf("Error %d in recv %lu : %s\n", result, rank,
                   taktuk_error_msg(result));
          }
        else
          {
            printf("Received [%s] and [%s] from %lu\n",
                   (char *) vecteur[0].iov_base,
                   (char *) vecteur[1].iov_base, from);
          }
      }

    return 0;
  }
