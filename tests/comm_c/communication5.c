/******************************************************************************
*                                                                             *
*  TakTuk, a middleware for adaptive large scale parallel remote executions   *
*  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        *
*                                                                             *
*  This program is free software; you can redistribute it and/or modify       *
*  it under the terms of the GNU General Public License as published by       *
*  the Free Software Foundation; either version 2 of the License, or          *
*  (at your option) any later version.                                        *
*                                                                             *
*  This program is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with this program; if not, write to the Free Software                *
*  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA *
*                                                                             *
*  Contact: Guillaume.Huard@imag.fr                                           *
*           ENSIMAG - Laboratoire d'Informatique de Grenoble                  *
*           51 avenue Jean Kuntzmann                                          *
*           38330 Montbonnot Saint Martin                                     *
*                                                                             *
******************************************************************************/

#include <taktuk.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

struct sender_parameters {
  char *message;
  int iter;
  unsigned long rank;
  unsigned long count;
  unsigned long dest;
} sender_parameters;

struct receiver_parameters {
  int iter;
  unsigned long rank;
  unsigned long count;
  struct timeval timeout;
} receiver_parameters;

void *sender(void *args)
  {
    struct sender_parameters *p = (struct sender_parameters *) args;
    int result;
    char my_buffer[1024];
    int i;
    char tid[32];

    strcpy(my_buffer, p->message);
    sprintf(tid," (thread %ld)",(long int) pthread_self());
    strcat(my_buffer, tid);

    srand((long int) pthread_self());
    for (i=0; i<p->iter; i++)
      {
        sleep(rand()%3);
        result = taktuk_send(p->dest, TAKTUK_TARGET_ANY, my_buffer,
                                                          strlen(my_buffer)+1);
        if (result)
          {
            printf("Error %d in send %lu (thread %ld): %s\n", result,
                p->rank, (long int) pthread_self(), taktuk_error_msg(result));
          }
      }
    pthread_exit(0);
  }

void *receiver(void *args)
  {
    struct receiver_parameters *p = (struct receiver_parameters *) args;
    char my_buffer[1024];
    size_t length;
    int i;
    unsigned long from, rank;
    int result;

    for (i=0; i<p->iter; i++)
      {
        result = taktuk_get("rank", &rank);
        if (result)
          {
            fprintf(stderr, "Invalid rank: %s, do you use TakTuk ?\n",
                    taktuk_error_msg(result));
            pthread_exit((void *)1);
          }
        result = taktuk_recv(&from, my_buffer, &length, &p->timeout);
        if (result)
          {
            printf("Error %d in recv %lu (thread %ld): %s\n", result,
                   rank, (long int) pthread_self(), taktuk_error_msg(result));
          }
        else
          {
            printf("Received [%s] from %lu (thread %ld)\n",
                   my_buffer,
                   from, (long int) pthread_self());
          }
      }
    pthread_exit(0);
  }

int main(int argc, char *argv[])
  {
    char buffer[128];
    unsigned long rank, count;
    int nb_threads, i, pos;
    pthread_t *threads;
    int result;

    if (argc != 4)
      {
        fprintf(stderr, "Invalid arguments number (threads, count, timeout)\n");
        return 1;
      }

    nb_threads = atoi(argv[1]);
    
    assert(!taktuk_init_threads());
    result = taktuk_get("rank", &rank);
    if (!result)
        result = taktuk_get("count", &count);
    if (result)
      {
        fprintf(stderr, "Invalid rank or count: %s, do you use TakTuk ?\n",
                taktuk_error_msg(result));
        return 1;
      }

    sender_parameters.message = buffer;

    sender_parameters.iter = atoi(argv[2]);
    sender_parameters.rank = rank;
    sender_parameters.count = count;
    sender_parameters.dest = 3-rank;

    receiver_parameters.iter = atoi(argv[2]);
    receiver_parameters.rank = rank;
    receiver_parameters.count = count;
    receiver_parameters.timeout.tv_sec = atoi(argv[3]);
    receiver_parameters.timeout.tv_usec = 0;

    strcpy(buffer, "Mon super message");

    printf("I'm process %lu among %lu\n", rank, count);
    fflush(stdout);

    threads = malloc(sizeof(pthread_t)*nb_threads*2);
    assert(threads);
    pos = 0;

    for (i=0; i<nb_threads; i++)
      {
        assert(!pthread_create(&threads[pos++],NULL,sender,
                                                          &sender_parameters)); 
        assert(!pthread_create(&threads[pos++],NULL,receiver,
                                                        &receiver_parameters)); 
      }

    printf("Threads created\n");
    fflush(stdout);

    for (i=0; i<2*nb_threads; i++)
      {
        assert(!pthread_join(threads[i], NULL));
      }
    assert(!taktuk_leave_threads());

    return 0;
  }
