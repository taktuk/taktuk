###############################################################################
#                                                                             #
#  TakTuk, a middleware for adaptive large scale parallel remote executions   #
#  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        #
#                                                                             #
#  This program is free software; you can redistribute it and/or modify       #
#  it under the terms of the GNU General Public License as published by       #
#  the Free Software Foundation; either version 2 of the License, or          #
#  (at your option) any later version.                                        #
#                                                                             #
#  This program is distributed in the hope that it will be useful,            #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#  GNU General Public License for more details.                               #
#                                                                             #
#  You should have received a copy of the GNU General Public License          #
#  along with this program; if not, write to the Free Software                #
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA #
#                                                                             #
#  Contact: Guillaume.Huard@imag.fr                                           #
#           Laboratoire LIG - ENSIMAG - Antenne de Montbonnot                 #
#           51 avenue Jean Kuntzmann                                          #
#           38330 Montbonnot Saint Martin                                     #
#           FRANCE                                                            #
#                                                                             #
###############################################################################

0:TITLE:C COMMUNICATION TESTS

1:TITLE:P2P, Batch, local file, no propagation
1:CMD:$TAKTUK -m localhost -m localhost `echo_local_c_file | flatten`

2:TITLE:PING PONG, Batch, local file, propagation
2:CMD:$TAKTUK_RELATIVE -s -m localhost -m localhost `echo_local_c_file 3 10 | flatten`

3:TITLE:PING PONG, Interactive, local file, no propagation
3:INC:echo_local_c_file.sh
3:CMD:echo_local_c_file 3 10 '|' $TAKTUK -m localhost -m localhost

4:TITLE:P2P, Interactive, local file, propagation
4:INC:echo_local_c_file.sh
4:CMD:echo_local_c_file '|' $TAKTUK_RELATIVE -s -m localhost -m localhost

5:TITLE:RING, Interactive, local file, propagation
5:INC:echo_local_c_file.sh
5:CMD:echo_local_c_file 2 '|' $TAKTUK_RELATIVE -s `generate_hosts 10`

6:TITLE:MULTITHREADED, Large count simultaneous bidirectional transfers
6:CMD:$TAKTUK `generate_hosts 2` `echo_local_c_file 5 2 10 0 | flatten`

7:TITLE:MULTITHREADED, Many threads simultaneous bidirectional transfers
7:CMD:$TAKTUK `generate_hosts 2` `echo_local_c_file 5 10 2 0 | flatten`

8:TITLE:MULTITHREADED, Timeouted simultaneous bidirectional transfers
8:CMD:$TAKTUK `generate_hosts 2` `echo_local_c_file 5 3 3 1 | flatten`

9:TITLE:MULTITHREADED, Timeouted simultaneous bidirectional vectorial transfers
9:INC:echo_local_c_file.sh
9:CMD:echo_local_c_file 4 3 3 1 '|' $TAKTUK_RELATIVE -s `generate_hosts 2`
