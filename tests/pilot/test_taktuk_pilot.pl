#!/usr/bin/perl -w -I../../Perl-Module/lib
use strict;

use TakTuk::Pilot;

our @line_counter;

sub output_callback(%) {
    my %parameters = @_;
    my $field = $parameters{fields};
    my $rank = $field->{rank};
    my $argument = $parameters{argument};

    $argument->[$rank] = 1 unless defined($argument->[$rank]);
    print "$field->{host}-$rank : $argument->[$rank] > $field->{line}\n";
    $argument->[$rank]++;
}

sub user_input_callback(%) {
    my %parameters = @_;
    my $taktuk = $parameters{taktuk};
    my $descriptor = $parameters{filehandle};
    my $buffer;

    my $result = sysread($descriptor, $buffer, 1024);
    warn "Read error $!" if not defined($result);
    # basic parsing, we assume input is buffered on a line basis
    chomp($buffer);

    if (length($buffer)) {
        print "Executing $buffer\n";
        $taktuk->send_command("broadcast exec [ $buffer ]");
    }
    if (not $result) {
        print "Terminating\n";
        $taktuk->remove_descriptor(type=>'read', filehandle=>$descriptor);
        $taktuk->send_termination();
    }
}

die "This script requieres as arguments hostnames to contact\n"
    unless scalar(@ARGV);

my $taktuk = TakTuk::Pilot->new();
$taktuk->add_callback(callback=>\&output_callback, stream=>'output',
                      argument=>\@line_counter,
                      fields=>['host', 'rank', 'line']);
$taktuk->add_descriptor(type=>'read', filehandle=>\*STDIN,
                        callback=>\&user_input_callback);
$taktuk->run(command=>"../../taktuk -s -m ".join(" -m ", @ARGV));
