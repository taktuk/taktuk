#!/usr/bin/perl -w -I../../Perl-Module/lib
use strict;

use TakTuk::Pilot;

sub output_callback(%) {
    my %parameters = @_;
    my $field = $parameters{fields};

    print "Host $field->{host}, rank $field->{rank} said : $field->{line}\n";
}

sub message_callback(%) {
    my %parameters = @_;
    my $field = $parameters{fields};
    my $from = $field->{from};
    my $taktuk = $parameters{taktuk};

    if ($field->{line} eq "end") {
        $taktuk->send_termination;
    } else {
        print "Peer $from sent me the message : $field->{line}\n";
        print "Bouncing the message to the same host\n";
        $taktuk->send_command("$from message [ $field->{line} ]");
    }
}

die "This script requieres as arguments hostnames to contact\n"
    unless scalar(@ARGV);

my $taktuk = TakTuk::Pilot->new();
$taktuk->add_callback(callback=>\&output_callback, stream=>'output',
                      fields=>['host', 'rank', 'line']);
$taktuk->add_callback(callback=>\&message_callback, stream=>'message',
                      fields=>['from', 'line']);
$taktuk->send_command(
                 "broadcast taktuk_perl [ -w -- $ENV{PWD}/$ENV{srcdir}/mirror.pl 1 2 3 ]");
$taktuk->send_command("broadcast message [ Salut ]");
print "Starting taktuk...\n";
$taktuk->run(command=>"../../taktuk -s -m ".join(" -m ", @ARGV));
