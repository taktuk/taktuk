###############################################################################
#                                                                             #
#  TakTuk, a middleware for adaptive large scale parallel remote executions   #
#  deployment. Perl implementation, copyright(C) 2006 Guillaume Huard.        #
#                                                                             #
#  This program is free software; you can redistribute it and/or modify       #
#  it under the terms of the GNU General Public License as published by       #
#  the Free Software Foundation; either version 2 of the License, or          #
#  (at your option) any later version.                                        #
#                                                                             #
#  This program is distributed in the hope that it will be useful,            #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#  GNU General Public License for more details.                               #
#                                                                             #
#  You should have received a copy of the GNU General Public License          #
#  along with this program; if not, write to the Free Software                #
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA #
#                                                                             #
#  Contact: Guillaume.Huard@imag.fr                                           #
#           Laboratoire LIG - ENSIMAG - Antenne de Montbonnot                 #
#           51 avenue Jean Kuntzmann                                          #
#           38330 Montbonnot Saint Martin                                     #
#           FRANCE                                                            #
#                                                                             #
###############################################################################

=pod TakTuk communication layer interface documentation (Perl interface)

=begin html

<center><h1>USER MANUAL</h1></center>

=end html

=head1 NAME

TakTuk - Perl module that provides an interface to C<taktuk(1)> communication
facilities

=head1 SYNOPSIS

  use TakTuk;
  
  my $rank = TakTuk::get('rank');
  my $count = TakTuk::get('count');
  
  print "I'm process $rank among $count\n";
  
  if ($rank > 1) {
      my ($from, $message) = TakTuk::recv();
      if (not defined($message)) {
          print "Trying to recv: ",
                TakTuk::error_msg($TakTuk::error), "\n";
      } else {
          print "$rank received $message from $from\n";
      }
  }
  
  sleep 1;
  my $next = $rank+1;
  $next = 1 if ($next > $count);
  if (not TakTuk::send(to=>$next, body=>"[Salut numero $rank]")) {
      print "Trying to send to $next: ",
            TakTuk::error_msg($TakTuk::error), "\n";
  }
  
  if ($rank == 1) {
      my ($from, $message) = TakTuk::recv(timeout=>5);
      if (not defined($message)) {
          print "Trying to recv :",
                TakTuk::error_msg($TakTuk::error), "\n";
      } else {
          print "$rank received $message from $from\n";
      }
  }

=head1 DESCRIPTION

The B<TakTuk> communication layer Perl interface provides a way for programs
executed using the C<taktuk(1)> command to exchange data. It is based on a
simple send/receive model using multicast-like sends and optionally timeouted
receives.  This is only designed to be a control facility, in particular this
is not a high performance communication library.

The Perl communication interface for B<TakTuk> is made of functions that can be
called by scripts executed using the C<taktuk_perl> command of the B<TakTuk>
engine (preferred way, less installation requirements on remote machines) or
using the B<TakTuk> Perl module provided with the B<TakTuk> distribution.
These functions are:

=over

=item B<TakTuk::get($)>

gets some information from B<TakTuk>. Currently available information includes
'target', 'rank', 'count', 'father', 'child_min' and 'child_max'. This is a
better way to get this information than environment variables as its takes
into account renumbering that might occur after process spawn.

=item B<TakTuk::send(%)>

sends a scalar to a single peer or a set specification (see C<taktuk(1)> for
information about set specifications).  The two mandatory fields in the
arguments are C<to> (with a set specification) and C<body>. Optionally, a field
C<target> might be given. Returns an undefined value upon error.

=item B<TakTuk::recv(%)>

blocks until the reception of a message. Returns a list of two elements:
the logical number of the source of the message and the message itself.
Accepts an optional C<timeout> argument with a numeric value.
Returns an empty list upon error.

=back

When an error occur, all these functions set the variable C<$TakTuk::error>
to the numeric code of the error that occured. A textual description of the
error is provided by the function C<TakTuk::error_msg($)> that takes the error
code as an argument.

Error codes are the following :

=over

=item TakTuk::ESWRIT

a call to C<TakTuk::syswrite> failed. This is due to a C<syswrite> error
different than C<EAGAIN>. The code should be accessible using C<$!>.

=item TakTuk::EFCLSD

the communication channel to the B<TakTuk> engine has been closed. This
typically occur when shutting down the logical network (using Ctrl-C on root
node for instance).

=item TakTuk::ESREAD (C<TakTuk::recv> only)

a call to C<sysread> failed (the code should be accessible using C<$!>).

=item TakTuk::EARGTO (C<TakTuk::send> only)

C<to> field missing in the arguments.

=item TakTuk::EARGBD (C<TakTuk::send> only)

C<body> field missing in the arguments.

=item TakTuk::ETMOUT (C<TakTuk::recv> only)

The call to C<TakTuk::recv> timeouted. This only occur when giving a C<timeout>
field as C<TakTuk::recv> argument.

=begin comment

=item TakTuk::EINVST (C<TakTuk::send> only)

The set specification given as a destination to the C<TakTuk::send> function is
not correct.

=end comment

=back

Finally, the B<TakTuk> Perl module defines some constants which value match the
different states reported by the stream C<state> (see C<taktuk(1)> for details
about this stream). These constant are the following:

  TakTuk::TAKTUK_READY
  TakTuk::TAKTUK_NUMBERED
  TakTuk::TAKTUK_TERMINATED
  TakTuk::CONNECTION_FAILED
  TakTuk::CONNECTION_INITIALIZED
  TakTuk::CONNECTION_LOST
  TakTuk::COMMAND_STARTED
  TakTuk::COMMAND_FAILED
  TakTuk::COMMAND_TERMINATED
  TakTuk::UPDATE_FAILED
  TakTuk::PIPE_STARTED
  TakTuk::PIPE_FAILED
  TakTuk::PIPE_TERMINATED
  TakTuk::FILE_RECEPTION_STARTED
  TakTuk::FILE_RECEPTION_FAILED
  TakTuk::FILE_RECEPTION_TERMINATED
  TakTuk::FILE_SEND_FAILED
  TakTuk::INVALID_TARGET
  TakTuk::NO_TARGET
  TakTuk::MESSAGE_DELIVERED
  TakTuk::INVALID_DESTINATION
  TakTuk::UNAVAILABLE_DESTINATION

=head1 SEE ALSO

C<tatkuk(1)>, C<taktukcomm(3)>, C<TakTuk::Pilot(3)>

=head1 AUTHOR

The original concept of B<TakTuk> has been proposed by Cyrille Martin in his PhD thesis. People involved in this work include Jacques Briat, Olivier Richard, Thierry Gautier and Guillaume Huard.

The author of the version 3 (perl version) and current maintainer of the package is Guillaume Huard.

=head1 COPYRIGHT

The C<TakTuk> communication interface library is provided under the terms
of the GNU General Public License version 2 or later.

=cut

