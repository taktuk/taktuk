#!/usr/bin/perl
# Test code aiming at evaluating the cost of sysread system call
# This version reads byte by byte
# Comparison with first version demonstrate the interest of reimplementing
# some basic buffering in Taktuk connectors

my $iterations=$ARGV[0];

my ($un, $deux);
pipe($deux, $un);
my $pid;
my ($buffer1, $buffer2);
if ($pid = fork())
{
  my $number = 1;
  close($un);
  #print "MON PID : $pid\n";
  for (my $i=0;$i<$iterations;$i++)
    {
      for (my $j=0;$j<255;$j++)
        {
          $number = sysread($deux,$buffer2,1);
          #print "$number octets lus : ",$buffer2;
          #print "Caractere : ",$buffer2,"\n";
        }
    }
}
else
{
  close($deux);
  $buffer1 = "abcdefghijklmnopqrstuvwxyz";
  for (my $i=0;$i<4;$i++)
    {
      $buffer1 = $buffer1.$buffer1;
    }
  #print "MON PID : $pid\n";
  open(STDOUT,">&",$un);
  close($un);
  for (my $i=0;$i<$iterations;$i++)
    {
      syswrite(STDOUT,$buffer1,255);
    }
  exit 0;
}
