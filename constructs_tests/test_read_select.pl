#!/usr/bin/perl
# Test code for the evaluation of threads efficiency
# This version uses only a single select to read the result of the remote execution
# of several 'ls' commands
# This is the best performing of the three versions

use strict;
use IO::Select;

my $num_commands = 64;
my $select = IO::Select->new;
my $i;
for ($i=0; $i<$num_commands; $i++)
  {
    my $fd;
    open $fd,"ls|" or die $!;
    $select->add($fd);
  }
while ($i)
  {
    my @select_result = $select->can_read;
    while (scalar(@select_result))
      {
        my $buffer;
        my $fd = shift @select_result;
        my $result = sysread($fd, $buffer, 4096);
        if ($result > 0)
          {
            print $buffer;
          }
        elsif ($result == 0)
          {
            $select->remove($fd);
            close($fd);
            $i--;
          }
        else
          {
            die $!;
          }
      }
  }
  
