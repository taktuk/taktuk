#!/usr/bin/perl
# Threads testing code for possible use in Taktuk
# This is a producer-consumer tested in Perl 5.8.6
# Characteristics :
# - Despite what I've found in the Perl documentation, modern versions of Perl
#   seem to share blessing among threads
# - Sharing do not dereference references, i.e. it is limited to flat structures
#   (this explain why each new produced object has to be shared explicitely

use strict;

package container;

sub new()
{
  my $data = {'data'=>3};
  bless($data);
  return($data);
}

sub set_data($)
{
  my $self = shift;
  $self->{'data'} = shift;
}

sub get_data()
{
  my $self = shift;
  return $self->{'data'};
}

package prodcons;

use threads;
use threads::shared;

sub new()
{
  my $data = &share([]);
  bless($data);
  return($data);
}

sub prod($)
{
  my $self = shift;
  lock($self);
  push @$self, shift;
  cond_signal($self);
}

sub cons()
{
  my $self = shift;
  lock($self);
  while (not scalar(@$self))
    {
      cond_wait($self);
    }
  return shift @$self;
}

package test;

use threads;
use threads::shared;

our $num_elements = 10;
our $num_threads = 10;

sub producer($)
{
  my $prodcons = shift;
  my $container;
  my $self = threads->self;

  for (my $i=0; $i<$num_elements; $i++)
    {
      print ($self->tid.": I will enqueue ".$i*$i."\n");
      $container = container::new;
      share($container);
      $container->set_data($i*$i);
      $prodcons->prod($container);
      $self->yield;
    }
}

sub consumer($)
{
  my $prodcons = shift;
  my $container;
  my $self = threads->self;

  for (my $i=0; $i<$num_elements; $i++)
    {
      $container = $prodcons->cons;
      print ($self->tid.": I have dequeued ".$container->get_data."\n");
      $self->yield;
    }
}

my $prodcons = prodcons::new;

my @producers;
my @consumers;
for (my $i=0; $i<$num_threads; $i++)
  {
    push @consumers, threads->create("consumer",$prodcons);
  }
for (my $i=0; $i<$num_threads; $i++)
  {
    push @producers, threads->create("producer",$prodcons);
  }

while (scalar(@producers))
  {
    my $producer = shift @producers;
    $producer->join;
  }
while (scalar(@consumers))
  {
    my $consumer = shift @consumers;
    $consumer->join;
  }
print "End of program\n";
