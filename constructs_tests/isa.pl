#!/usr/bin/perl
# Test code for possible future options parser (handler should be able to make
# the difference between refs to a variable and to a function...)
use strict;

my @essai = (1, 3, 6);
my $tata = "salut";
my $file;
my $toto;
open $file, "<", $0 or &diagnostic::error("Can't open $0");
sub prout()
{ print "salut"; }

print ref(\@essai);
print ref($file);
print UNIVERSAL::isa($file,'GLOB'), "\n";
print ref(\&prout), "\n";
print "Tata: ",ref($tata),"\n";
print UNIVERSAL::isa(\$toto,'SCALAR'), "\n";
print UNIVERSAL::isa(\&prout,'SCALAR'), "\n";
print UNIVERSAL::isa(\$toto,'CODE'), "\n";
print UNIVERSAL::isa(\&prout,'CODE'), "\n";
