#!/usr/bin/perl -w
use IO::Select;
use Socket;

my $select = IO::Select->new;
our $root=1;

sub my_print($)
{
  my $message = shift;
  syswrite(STDOUT,$message,length($message))
}

sub no_flush($)
{
  my $old_fd = select(shift);
  $| = 1;
  select($old_fd);
}

no_flush(\*STDOUT);


sub main_loop()
{
  while (1)
    {
      my @select_result =
          IO::Select::select($select,undef,$select,1);
      if (scalar(@select_result))
        {
          my $buffer;
          my $read_handles = $select_result[0];
          foreach my $handle (@$read_handles)
            {
              my $result = sysread($handle, $buffer, 1024);
              exit 0 if $result == 0;
              $buffer = "Received : ".$buffer if ($root);
              #my_print("Received : ") if ($root);
              die "Can't sysread" if $result < 0;
              #syswrite(STDOUT,$buffer,$result);
              my_print($buffer);
            }
        }
      else
        {
          # Here we might have been thrown out of select by a timeout
          # but we have to test for error conditions (just in case)
          if ($!{EBADF})
            {
              my $error_msg="Selected an invalid descriptor... Very bad\n".
                             "Registered handles = ";
              foreach my $handle ($select->handles)
                {
                  $error_msg.="$handle (".fileno($handle).") \n";
                }
              my_print($error_msg);
            }
          elsif ($!{EINTR})
            {
              my_print("Select exited because of a signal\n");
            }
          elsif ($!{EINVAL})
            {
              my_print("Invalid time limit for select\n");
            }
          else
            {
              if ($root)
                {
                  my_print ("Father : Select timeout\n");
                }
              else
                {
                  my_print ("Select timeout\n");
                }
            }
        }
    }
}

if ($ARGV[0] eq "-r")
  {
    $root=0;
    $select->add(\*STDIN);
    main_loop;
  } 

while (scalar(@ARGV))
  {
    my $host = shift @ARGV;
    my ($father,$child) = (undef,undef);

    socketpair($father, $child, AF_UNIX, SOCK_STREAM, PF_UNSPEC)
        or die "Can't socketpair";
  
    defined(my $pid = fork()) or die "Can't fork";
    if ($pid)
      {
        close($child);
        $select->add($father);
      }
    else
      {
        close($father);
        open(STDIN,"<&",$child) or die $!;
        open(STDOUT,">&",$child) or die $!; 
        exec("ssh -o StrictHostKeyChecking=no -o BatchMode=yes $host ./essai_select.pl -r");
      }
  }
main_loop;
