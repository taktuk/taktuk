#!/usr/bin/perl -w
use strict;

package toto;

sub method1 () {
    my $self = shift;
    my @infos = caller(0);

    print $self->{'valeur'}, "\n";
    print join(" ",@infos[1..4]), "\n";
}

sub method2 () {
    my $self = shift;
    my @infos = caller(0);

    print $self->{'valeur'}, "\n";
    print join(" ",@infos[1..4]), "\n";
}

sub new ($) {
    my $data = {'valeur'=>shift};
    bless $data;
    return $data;
}

package tata;

our @ISA = qw(toto);

sub method2 () {
    my @infos = caller(0);

    print "Pas de valeur\n";
    print join(" ",@infos[1..4]), "\n";
}

sub new ($) {
    my $data = {'valeur'=>shift};
    bless $data;
    return $data;
}

package main;

my $objet1 = toto::new(5);
my $objet2 = tata::new(3);

$objet1->method1;
$objet1->method2;
$objet2->method1;
$objet2->method2;
