#!/usr/bin/perl
# Test code for socketpair system call
# should replace pipes in Taktuk (got do check portability before doing the change)

use Socket;

my ($un, $deux);
socketpair($un, $deux, AF_UNIX, SOCK_STREAM, PF_UNSPEC) or die $!;
#pipe ($deux, $un) or die $!;
my $pid;
my ($buffer1, $buffer2);
if ($pid = fork())
{
  close($un);
  print "MON PID : $pid\n";
  while (1)
    {
      sysread($deux,$buffer2,128);
      print $buffer2;
      sleep 1;
    }
}
else
{
  close($deux);
  $buffer1="Pong\n";
  print "MON PID : $pid\n";
  open(STDOUT,">&",$un);
  close($un);
  while (1)
    {
      syswrite(STDOUT,$buffer1,6);
    }
}
