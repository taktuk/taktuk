#!/usr/bin/perl
# Test code for the evaluation of threads efficiency
# This version uses only threads to read the result of the remote execution
# of several 'ls' commands
# This is the worst performing

use strict;
use threads;
use threads::shared;

my $num_commands = 100;
my $count : shared = 0;

sub thread_func()
{
  my $fd;
  open $fd,"ls|";
  lock($count);
  $count++;
  print "Opened $count\n";
  if ($count < $num_commands)
    {
      cond_wait($count);
    }
  else
    {
      cond_broadcast($count);
    }
  my $buffer;
  my $result = sysread($fd,$buffer,4096);
  while ($result > 0)
    {
      print $buffer;
      $result = sysread($fd, $buffer, 4096);
    }
  die $! if ($result < 0);
  close $fd;
}

my $i;
my @threads;
for ($i=0; $i<$num_commands; $i++)
  {
    $threads[$i] = threads->create("thread_func");
  }
for ($i=0; $i<$num_commands; $i++)
  {
    $threads[$i]->join;
  }
