#!/usr/bin/perl
# Test code aiming at evaluating the cost of sysread system call
# This version reads by blocks of 255 bytes
# Comparison with second version demonstrate the interest of reimplementing
# some basic buffering in Taktuk connectors

my $iterations = $ARGV[0];

my ($un, $deux);
pipe($deux, $un);
my $pid;
my ($buffer1, $buffer2);
if ($pid = fork())
{
  my $number = 1;
  close($un);
  #print "MON PID : $pid\n";
  for (my $i=0;$i<$iterations;$i++)
    {
      my $car;
      my $j=0;

      $number = sysread($deux,$buffer2,255);
      #print "$number octets lus : ",$buffer2;
      while ($j<255)
        {
          $car = substr $buffer2, $j, 1;
          $j++;
          #print "Caract�re : ",$car,"\n";
        }
    }
}
else
{
  close($deux);
  $buffer1 = "abcdefghijklmnopqrstuvwxyz";
  for (my $i=0;$i<4;$i++)
    {
      $buffer1 = $buffer1.$buffer1;
    }
  #print "MON PID : $pid\n";
  open(STDOUT,">&",$un);
  close($un);
  for (my $i=0;$i<$iterations;$i++)
    {
      syswrite(STDOUT,$buffer1,255);
    }
  exit 0;
}
