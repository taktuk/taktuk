#!/usr/bin/perl
# Test code for the evaluation of threads efficiency
# This version mix threads and select to read the result of the remote execution
# of several 'ls' commands
# Performance is inbetween the purely threaded version and the version using only
# a single select

use strict;
use IO::Select;
use threads;

my $num_threads = 8;
my $num_commands = 8;

sub launch_commands ()
{
  my $select = IO::Select->new;
  my $i;
  for ($i=0; $i<$num_commands; $i++)
    {
      my $fd;
      open $fd,"ls|";
      $select->add($fd);
    }
  while ($i)
    {
      my @select_result = $select->can_read;
      while (scalar(@select_result))
        {
          my $buffer;
          my $fd = shift @select_result;
          my $result = sysread($fd, $buffer, 4096);
          if ($result > 0)
            {
              print $buffer;
            }
          elsif ($result == 0)
            {
              $select->remove($fd);
              close($fd);
              $i--;
            }
          else
            {
              die $!;
            }
        }
    }
}

my $i;
my @threads;
for ($i=0; $i<$num_threads; $i++)
  {
    $threads[$i] = threads->create("launch_commands");
  }
for ($i=0; $i<$num_threads; $i++)
  {
    $threads[$i]->join;
  }
