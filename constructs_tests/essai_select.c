#include <string.h>
#include <unistd.h>
#include<sys/types.h>
#include <sys/time.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <stdio.h>

int root=1;
fd_set my_set;
int nfds;
int fd_array[1024];

void my_print(char *buffer)
{
  write(1, buffer, strlen(buffer));
}

void main_loop()
{
  struct timeval timeout;
  fd_set read_set, except_set;
  int i;
  char buffer[1024], message[1280];
  int result;

  while (1)
    {
      // FD_COPY not portable (e.g. on itanium)
      FD_ZERO(&read_set);
      FD_ZERO(&except_set);
      for (i=0; i<nfds; i++)
        {
          FD_SET(fd_array[i],&read_set);
          FD_SET(fd_array[i],&except_set);
        }
      timeout.tv_sec = 1;
      timeout.tv_usec = 0;
      result = select(nfds, &read_set, NULL, &except_set, &timeout);
      if (result > 0)
        {
          for (i=0; i<nfds; i++)
            {
              if (FD_ISSET(fd_array[i], &read_set))
                {
                  result = read(fd_array[i], buffer, 1024);
                  if (result > 0)
                    {
                      strcpy(message, "Received : ");
                      strncat(message, buffer, result);
                      my_print(message);
                    }
                  else 
                    {
                      if (result < 0)
                        {
                          my_print("Read error\n");
                          exit(1);
                        }
                      else
                        {
                          my_print("End of file\n");
                        }
                    }
                }
            }
        }
      else
        {
          if (result < 0)
            {
              switch(errno)
                {
                case EBADF:
                  my_print("Bad FD\n");
                  break;
                case EINTR:
                  my_print("Interrupted by a signal\n");
                  break;
                case EINVAL:
                  my_print("Invalid time limit\n");
                default:
                  my_print("Unknown error");
                }
                exit(1);
            }
          else
            {
              if (root)
                  my_print("Father : Select timeout\n");
              else
                  my_print("Select timeout\n");
            }
        }
    }
}

int main(int argc, char *argv[])
{
  int i;
  int pair[2];

  if (strcmp(argv[1],"-r") == 0)
    {
      root=0;
      fd_array[0] = 0;
      nfds = 1;
      main_loop();
    }
  else
    {
      printf("%d\n",FD_SETSIZE);
      nfds=0;
      for (i=1; i<argc; i++)
        {
          if (socketpair(AF_UNIX, SOCK_STREAM, PF_UNSPEC, pair) < 0)
            {
              my_print("Socketpair/Pipe error\n");
              exit(1);
            }
          switch(fork())
            {
            case -1:
              my_print("Fork error\n");
              exit(1);
            case 0:
              close(pair[0]);
              dup2(pair[1],1);
              close(pair[1]);
              execlp("ssh", "ssh", "-o", "StrictHostKeyChecking=no", "-o",
                  "BatchMode=no", argv[i], "./essai_select", "-r", (char *)0);
              my_print("Exec error\n");
              exit(1);
            default:
              close(pair[1]);
              fd_array[i-1] = pair[0];
              if (nfds <= pair[0])
                  nfds = pair[0] + 1;
            }
        }
      main_loop();
    }
  return 0;
}
