#!/usr/bin/perl -w
use strict;

my $text = `cat $ARGV[0]`;
$text =~ s/\n\s*{/ {/mg;
$text =~ s/  }/}/mg;
$text =~ s/}\s*\n\s*else/} else/mg;
$text =~ s/}\s*\n\s*elsif/} elsif/mg;
$text =~ s/^  ((?:    )*)}/    $1}/mg;
print $text;
