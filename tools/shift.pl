#!/usr/bin/perl
use bytes;

my $source="A-Za-z0-9 ,.;:?!";
my $dest="M-Za-z0-9 ,.;:?!A-L";
my $mode;

if ($ARGV[0] eq "encode")
  {
    $mode = 0;
  }
elsif ($ARGV[0] eq "decode")
  {
    $mode = 1;
  }
else
  {
    die "Invalid mode";
  }
shift @ARGV;

my $line;
if ($mode)
  {
    while ($line = <>)
      {
        eval "\$line =~ tr/$dest/$source/";
        print $line;
      }
  }
else
  {
    while ($line = <>)
      {
        $line =~ tr/����������/aaeeeeiiou/;
        eval "\$line =~ tr/$source/$dest/";
        print $line;
      }
  }
